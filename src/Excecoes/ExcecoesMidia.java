package Excecoes;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExcecoesMidia {

	static Scanner scan = new Scanner(System.in);

	public static int pedeInt(String mensagem) {
		while (true) {
			try {

				System.out.println(mensagem);

				int i = scan.nextInt();

				scan.nextLine();

				return i;

			} catch (InputMismatchException e) {
				System.out.println("Voc� n�o digitou numeros..");

				scan.nextLine();

			}
		}

	}

	public static String pedeString(String mensagem) {
		String str = new String("");

		while (true) {

			System.out.print(mensagem);
			str = scan.nextLine();

			if (str.isEmpty()) {

				System.out.println("Este campo n�o pode ficar em branco");

			} else {

				return str;

			}
		}

	}

}
