
package visao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

import modelo.dados.ControlaEbook;

/**
 * Classe que contem o metodo main, instacia as interfaces de cada tipo de midia e exibe o menu principal para o usuario. 
 * @author Mario
 *
 */
public class InterfaceMidia {

	/**
	 * Metodo que inicia o programa de controle de midias.
	 * @param args
	 * @throws Exception
	 */
	public static void main (String []args) {

		InterfaceJogo JG = new InterfaceJogo();
		InterfaceEbook EB = new InterfaceEbook();
		InterfaceMusica MU = new InterfaceMusica();
		InterfaceFilmes FI = new InterfaceFilmes();
		@SuppressWarnings("unused")
		int entrada = 6, numSel;    
		numSel = 6;    
		do{                       
			System.out.println(
					"\n   MIDIAS "
							+ "\n"
							+ "\n"
							+ "\n   1. Jogos "
							+ "\n   2. Musicas "
							+ "\n   3. EBooks "
							+ "\n   4. Filmes "
                            + "\n   0. Voltar ao Menu ");
			try{
				@SuppressWarnings("resource")
				Scanner sc = new Scanner(System.in);  	
				numSel = sc.nextInt();
				switch (numSel) {
				case 1:
					try {
						JG.cadastro();
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}  
					System.out.println("-----------------");
					numSel = 6;
					break;                            
				case 2:
					MU.cadastro();
					System.out.println("-----------------");
					break;
				case 3:
					ControlaEbook cadastroEbook = new ControlaEbook();
					try{
						cadastroEbook.lerArquivo("ebook.txt");
					}catch(FileNotFoundException e){
						System.out.println("Arquivo n�o encontrado.");
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						EB.cadastro(cadastroEbook);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}  
					System.out.println("-----------------");
					numSel = 6;
					break;                            
				case 4:
					try {
						FI.cadastro();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println("-----------------");
					break;
				}
			}catch(InputMismatchException f){
				System.out.println("Informe uma op��o entre [0-4]");
			}
		}     while (numSel != 0);   
	}
}