package visao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import Excecoes.ExcecoesMidia;
import modelo.dados.ControlaMusica;
import modelo.dados.OrdenacaoEBuscaMusica;
import modelo.entidade.Musica;

/**
 * Classe de intera��o com o user
 * 
 * @author MatheusLe�o
 *
 */
public class InterfaceMusica {

	ControlaMusica musica = new ControlaMusica();
	OrdenacaoEBuscaMusica ordena = new OrdenacaoEBuscaMusica();
	private Scanner entrada;

	/**
	 * M�todo que exibe o menu com as op��es para o user
	 * 
	 */
	public void cadastro() {

		entrada = new Scanner(System.in);

		try {
			musica.lerArquivo("Musicas.bin");
		} catch (Exception e1) {

		}
		int opcao = 0;

		do {
			opcao = ExcecoesMidia.pedeInt("Menu Musica\n" + "1. Cadastrar Musica\n" + "2. Exibir musicas cadastradas\n"
					+ "3. alterar musica\n" + "4  excluir musica\n" + "5. Busca de Musicas por agrupamento\n."
					+ "0. Voltar para o Menu Principal\n");

			if (opcao == 1) {

				incluirMusica();

			} else if (opcao == 2) {
				ordena.ordenarTitulo();
				listarMusica();

			} else if (opcao == 3) {

				alterarMusica();

			} else if (opcao == 4) {

				excluirMusica();

			} else if (opcao == 5) {
				try {
					musica.criarArquivo("Musica.bin");
				} catch (IOException e) {
					
				}
				
				do{
				
					opcao = ExcecoesMidia.pedeInt("M�todos de busca: \n\n " + "1. Buscar por g�nero\n"
							+ "2. Buscar por Ano\n" + "3. Buscar por Autor(es)\n"
							+ "0. voltar  para o Menu de Musicas\n");

					String resposta;
					if (opcao == 1) {

						resposta = ExcecoesMidia.pedeString("Informe o G�nero da musica que deseja listar: ");

						System.out.println(ordena.buscaPorGenero(resposta));

					} else if (opcao == 2) {
						int res = ExcecoesMidia.pedeInt("Informe o Ano da musica que deseja listar: ");

						System.out.println(ordena.buscaPorAno(res));

					} else if (opcao == 3) {

						resposta = ExcecoesMidia.pedeString("Informe o Autor(es) da musica que deseja listar: ");

						System.out.println(ordena.buscaPorAutor(resposta));

					}
					
				}while(opcao != 0);
				

			}

		}while(opcao != 0 );

		try {
			musica.criarArquivo("Musicas.bin");
		} catch (FileNotFoundException exc) {
			System.err.println("Erro: Musica.txt n�o encontrado");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Informa��es gravadas com sucesso ! ");

	}

	/**
	 * M�todo para adicionar uma nova musica
	 */
	public void incluirMusica() {

		entrada = new Scanner(System.in);

		String nomeDoArquivo;
		String titulo;
		String descricao;
		String genero;
		int ano;
		int duracao;
		String idioma;
		String autor;
		String interprete;
		ArrayList<String> autores = new ArrayList<String>();
		ArrayList<String> interpretes = new ArrayList<String>();
		String resposta;

		nomeDoArquivo = ExcecoesMidia.pedeString("Informe o nome da Musica:");

		titulo = ExcecoesMidia.pedeString("Informe o titulo da Musica:");

		descricao = ExcecoesMidia.pedeString("Informe a descri��o da Musica:");
		
		genero = ExcecoesMidia.pedeString("Informe o genero:");
		
		idioma = ExcecoesMidia.pedeString("Informe o idioma:");
		
		int i = 0;
		do {

			autor = ExcecoesMidia.pedeString("Informe o nome do " + (i + 1) + "� autor: ");

			autores.add(autor);

			resposta = ExcecoesMidia.pedeString("Deseja adicionar outro autor ? S/N");
			i++;

		} while (resposta.equalsIgnoreCase("S"));

		i = 0;
		do {

			resposta = ExcecoesMidia.pedeString("Informe o nome do " + (i + 1) + " interprete: ");
			interprete = entrada.nextLine();
			interpretes.add(interprete);
			i++;
			resposta = ExcecoesMidia.pedeString("Deseja adicionar outro autor ? S/N");

		} while (resposta.equalsIgnoreCase("S"));

		duracao = ExcecoesMidia.pedeInt("Informe a dura��o: ");
		

		ano = ExcecoesMidia.pedeInt("Informe o ano: ");
		entrada.nextLine();

		Musica m = new Musica(nomeDoArquivo, titulo, descricao, genero, idioma, autores, interpretes, duracao, ano);

		if (musica.AdicionaMusica(m)) {
			System.out.println("Musica adicionada com sucesso ! \n");
		} else {
			System.out.println("Essa musica j� existe na Playlist !");
		}

	}

	/**
	 * m�todo de altera��o de uma determinada musica
	 */

	public void alterarMusica() {

		entrada = new Scanner(System.in);

		String nomeDoArquivo;
		String titulo;
		String descricao;
		String genero;
		int ano;
		int duracao;
		String idioma;
		String autor;
		String interprete;
		ArrayList<String> autores = new ArrayList<String>();
		ArrayList<String> interpretes = new ArrayList<String>();
		String resposta, res;

		System.out.println("Informe o novo t�tulo da musica a ser alterada: ");
		res = entrada.nextLine();

		if (musica.existe(res)) {

			nomeDoArquivo = ExcecoesMidia.pedeString("Informe o novo nome da Musica:");

			titulo = ExcecoesMidia.pedeString("Informe o novo titulo da Musica:");

			descricao = ExcecoesMidia.pedeString("Informe a nova descri��o da Musica:");
		
			genero = ExcecoesMidia.pedeString("Informe o novo genero:");
	
			idioma = ExcecoesMidia.pedeString("Informe o novo idioma:");
			
			int i = 0;
			do {

				autor = ExcecoesMidia.pedeString("Informe o novo nome do " + (i + 1) + "� autor: ");

				autores.add(autor);

				resposta = ExcecoesMidia.pedeString("Deseja adicionar outro autor ? S/N");
				i++;

			} while (resposta.equalsIgnoreCase("S"));

			i = 0;
			do {

				resposta = ExcecoesMidia.pedeString("Informe o novo nome do " + (i + 1) + " interprete: ");
				interprete = entrada.nextLine();
				interpretes.add(interprete);
				i++;
				resposta = ExcecoesMidia.pedeString("Deseja adicionar outro novo autor ? S/N");

			} while (resposta.equalsIgnoreCase("S"));

			duracao = ExcecoesMidia.pedeInt("Informe a nova dura��o: ");
			

			ano = ExcecoesMidia.pedeInt("Informe o novo ano: ");
			entrada.nextLine();

			Musica m = new Musica(nomeDoArquivo, titulo, descricao, genero, idioma, autores, interpretes, duracao, ano);

			if (musica.AdicionaMusica(m)) {
				System.out.println("Musica adicionada com sucesso ! \n");
			} else {
				System.out.println("Essa musica j� existe na Playlist !");
			}
		}

	}

	/**
	 * m�todo para excluir determinada m�sica
	 */

	public void excluirMusica() {
		entrada = new Scanner(System.in);
		String titulo;

		titulo = ExcecoesMidia.pedeString("Informe o t�tulo da musica que deseja exluir: ");
		
		if (musica.remove(titulo)) {

			System.out.println("Musica removida com sucesso !");
		} else {
			System.out.println("Musica n�o foi encontrada");
		}

	}

	/**
	 * m�todo para listar as musicas contidas no ArrayList caso n�o exista
	 * nenhuma musica cadastrada � perguntado ao usu�rio se ele deseja cadastrar
	 */

	public void listarMusica() {
		entrada = new Scanner(System.in);
		String resposta;

		if (musica.getMusicas() != null) {
			System.out.println(musica.getMusicas().toString());

			System.out.println("\n\n");
		} else {
			System.out.println("nenhuma musica encontrada !");
			System.out.println("Deseja cadastrar uma musica ? S/N");
			resposta = entrada.nextLine();
			if (resposta.equalsIgnoreCase("s")) {

				incluirMusica();
			}
		}
	}

}
