package visao;

/**
 * 
 */
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import modelo.dados.ControlaJogo;
import modelo.entidade.Jogo;

/**
 * 
 * @author eduardo
 * 
 */
public class InterfaceJogo {

	ControlaJogo jogo = new ControlaJogo();

	Scanner entrada = new Scanner(System.in);
/**
 * 
 */
	public void cadastro() {

		try {
			jogo.lerArquivo("Jogos.bin");
		} catch (Exception exc) {

		}

		int numSel;
		int numeroSel;

		do {
			System.out.println("\n   Jogos " + "\n" + "\n"
					+ "\n   1. Cadastrar " + "\n   2. Alterar "
					+ "\n   3. Listar " + "\n   4. Excluir " + "\n   5. Busca "

					+ "\n   0. Voltar ao Menu ");

			numSel = entrada.nextInt();

			switch (numSel) {

			case 1:
				incluirJogo();
				System.out.println("-----------------");

				break;
			case 2:
				alterarJogo();

				break;
			case 3:
				listarJogo();

				break;

			case 4:
				excluirJogo();

				break;
			case 5:
				do {
					System.out.println("\n   Buscas " + "\n" + "\n"
							+ "\n   1. Buscar por Titulo "
							+ "\n   2. Busca por Genero "
							+ "\n   3. Busca por ano "
							+ "\n   0. Voltar ao Menu ");

					numeroSel = entrada.nextInt();

					switch (numeroSel) {

					case 1:
						BuscaDeTitulo();

						break;
					case 2:
						BuscaDeGenero();

						break;
					case 3:
						BuscaDeAno();

						break;

					}

				} while (numeroSel != 0);

				break;

			}

		} while (numSel != 0);

		try {
			jogo.criarArquivo("Jogos.bin");
		} catch (FileNotFoundException exc) {
			System.err.println("Erro: Jogos.bin não encontrado");

		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Informações gravadas com sucesso ! ");

	}

	/**
 * 
 */
	public void incluirJogo() {

		entrada = new Scanner(System.in);

		ArrayList<String> autores = new ArrayList<String>();
		boolean sRede;
		String resposta;
		String nomeDoArquivo ;
		
				
		
	    nomeDoArquivo = Excecoes.ExcecoesMidia
				.pedeString("Informe o nome do arquivo:");
		

		String titulo = Excecoes.ExcecoesMidia
				.pedeString("Informe o titulo do jogo:");

		System.out.println("Informe a descrição do jogo:");
		String descricao = entrada.nextLine();

		System.out.println("Informe o genero do jogo:");
		String genero = entrada.nextLine();

		int i = 0;
		do {

			System.out.println("Informe o nome do " + (i + 1) + " autor: ");
			String autor = entrada.nextLine();
			autores.add(autor);

			System.out.println("Deseja adicionar outro autor ? S/N");
			resposta = entrada.nextLine();
			i++;

		} while (resposta.equalsIgnoreCase("S"));

		i = 0;

		int ano = Excecoes.ExcecoesMidia.pedeInt("Informe o ano do jogo: ");

		int nJogadores = Excecoes.ExcecoesMidia
				.pedeInt("Informe o numero de jogadores:");

		System.out.println("Informe se o jogo usa rede: sim ou não");
		if (entrada.next().equalsIgnoreCase("sim")) {
			sRede = true;
		} else {
			sRede = false;
		}

		Jogo j = new Jogo(nomeDoArquivo, titulo, descricao, genero, autores,
				ano, nJogadores, sRede);

		jogo.AdicionaJogo(j);

	}

	/**
 * 
 */
	public void alterarJogo() {

		entrada = new Scanner(System.in);

		ArrayList<String> autores = new ArrayList<String>();

		boolean sRede;
		String resposta;
		String res;

		System.out.println("Informe o titulo do jogo a ser alterado: ");
		res = entrada.nextLine();

		if (jogo.existe(res)) {

			String nomeDoArquivo = Excecoes.ExcecoesMidia
					.pedeString("Informe o nome do arquivo:");

			String titulo = Excecoes.ExcecoesMidia
					.pedeString("Informe o titulo do jogo:");

			System.out.println("Informe a descrição do jogo:");
			String descricao = entrada.nextLine();

			System.out.println("Informe o genero do jogo:");
			String genero = entrada.nextLine();

			int i = 0;
			do {

				System.out.println("Informe o nome do " + (i + 1) + " autor: ");
				String autor = entrada.nextLine();
				autores.add(autor);

				System.out.println("Deseja adicionar outro autor ? S/N");
				resposta = entrada.nextLine();
				i++;

			} while (resposta.equalsIgnoreCase("S"));

			i = 0;

			int ano = Excecoes.ExcecoesMidia.pedeInt("Informe o ano do jogo: ");

			int nJogadores = Excecoes.ExcecoesMidia
					.pedeInt("Informe o numero de jogadores:");

			System.out.println("Informe se o jogo usa rede: sim ou não");
			if (entrada.next().equalsIgnoreCase("sim")) {
				sRede = true;
			} else {
				sRede = false;
			}

			Jogo j = new Jogo(nomeDoArquivo, titulo, descricao, genero,
					autores, ano, nJogadores, sRede);

			jogo.alteraJogo(res, j);

		}
	}

/**
 * 
 */
	public void excluirJogo() {

		entrada = new Scanner(System.in);

		System.out.println("Informe o titulo do jogo que deseja exluir: ");
		String titulo = entrada.next();
		if (jogo.exclui(titulo)) {

			System.out.println("Jogo removido com sucesso !");
		} else {
			System.out.println("Jogo não foi encontrado");
		}

	}

/**
 * 
 */
	public void listarJogo() {
		entrada = new Scanner(System.in);
		String resposta;

		if (jogo.ListaJogos() != null) {
			System.out.println(jogo.ListaJogos().toString());
			System.out.println("\n\n");
		} else {
			System.out.println("nenhum jogo encontrado !");
			System.out.println("Deseja cadastrar um jogo ? S/N");
			resposta = entrada.nextLine();
			if (resposta.equalsIgnoreCase("s")) {

				incluirJogo();
			}
		}
	}

/**
 * 
 */
	public void BuscaDeTitulo() {
		entrada = new Scanner(System.in);
		if (jogo.ListaJogos() != null) {
		String resposta = Excecoes.ExcecoesMidia
				.pedeString("Informe o Titulo do Jogo que deseja listar: ");

		System.out.println(jogo.buscaPorTitulo(resposta));
		} else {

			System.out.println("nenhum jogo encontrado !");
			System.out.println("Deseja cadastrar um jogo ? S/N");
			String resposta = entrada.nextLine();
			if (resposta.equalsIgnoreCase("s"))

				incluirJogo();
		}
	}

/**
 * 
 */
	public void BuscaDeGenero() {
		entrada = new Scanner(System.in);
		if (jogo.ListaJogos() != null) {
		String resposta = Excecoes.ExcecoesMidia
				.pedeString("Informe o Genero do Jogo que deseja listar: ");

		System.out.println(jogo.buscaPorGenero(resposta));
		} else {

			System.out.println("nenhum jogo encontrado !");
			System.out.println("Deseja cadastrar um jogo ? S/N");
			String resposta = entrada.nextLine();
			if (resposta.equalsIgnoreCase("s"))

				incluirJogo();
		}
	}
	

/**
 * 
 */
	public void BuscaDeAno() {
		entrada = new Scanner(System.in);

		if (jogo.ListaJogos() != null) {
			int res = Excecoes.ExcecoesMidia
					.pedeInt("Informe o Ano do Jogo que deseja listar: ");

			System.out.println(jogo.buscaPorAno(res));

		} else {

			System.out.println("nenhum jogo encontrado !");
			System.out.println("Deseja cadastrar um jogo ? S/N");
			String resposta = entrada.nextLine();
			if (resposta.equalsIgnoreCase("s"))

				incluirJogo();
		}
	}
}
