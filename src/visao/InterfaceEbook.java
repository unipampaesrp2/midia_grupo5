package visao;

import java.io.File;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

import modelo.dados.ControlaEbook;
import modelo.entidade.Ebook;

/**
 * Esta classe realiza a interface com usu�rio atrav�s do System.out.print. 
 * @author Mario
 *
 */
public class InterfaceEbook {
	ControlaEbook cadastroEbook = new ControlaEbook();
	int anoAtual=2015;

	/**Este m�todo exibe o menu de op��es para o usu�rio escolher qual opera��o deseja realizar com os ebooks.
	 *    
	 */
	@SuppressWarnings({ "resource", "unused" })
	public void cadastro(ControlaEbook cadastroEbook) throws IOException  {
		this.cadastroEbook = cadastroEbook;
		int entrada = 6, numSel;    
		numSel = 6;    
		do{                       
			Scanner sc = new Scanner(System.in);
			System.out.println(
					"\n   Ebook "
							+ "\n"
							+ "\n"
							+ "\n   1. Cadastrar "
							+ "\n   2. Listar Cadastros "
							+ "\n   3. Alterar "
							+ "\n   4. Buscar "
							+ "\n   5. Excluir "
							+ "\n   6. Ordenar "
							+ "\n   7. Voltar ao Menu ");
			try{
				numSel = sc.nextInt();
				switch (numSel) {
				case 1:
					incluirEbook();  
					System.out.println("-----------------");
					numSel = 6;
					break;                            
				case 2:
					listarCadastros();
					numSel = 6;
					break;
				case 3:
					editaEbook();
					numSel = 6;
					break;
				case 4:
					busca();
					numSel = 6;
					break;
				case 5:
					excluirEbook();
					numSel = 6;
					break; 
				case 6:
					ordenar();
					numSel = 6;
					break;
				}
			}catch(InputMismatchException e){
				System.out.println("Informe um valor entre 1 e 7.");
			}
		}
		while (numSel != 7);
		cadastroEbook.ordena();
		try{
			cadastroEbook.criarArquivo();
		}catch(IOException e ){
			System.out.println("Arquivo n�o existente.");
		}
	}


	/**
	 * Este m�todo solicita ao usu�rio as informa��es sobre o ebook. 
	 */
	@SuppressWarnings("resource")
	public void incluirEbook() {
		Scanner entrada = new Scanner(System.in);
		String nomeDoArquivo;
		String titulo;
		String descricao;
		String genero;
		String autor;
		String autores = "";
		String local = null;
		String editora = null;
		int numeroDePaginas = 0,qtdAutores = 0;
		int ano = 0;
		File arquivo;  
		File diretorio = null;
		System.out.println("\n**************");
		System.out.println("3. EBOOK");
		System.out.println("1. CADASTRAR");
		System.out.println("**************");
		System.out.println("Informe o nome do arquivo do ebook: ");
		nomeDoArquivo = entrada.nextLine();
		if(cadastroEbook.existeFile(nomeDoArquivo)){
			System.out.println("Arquivo j� cadastrado.");
		}
		else{
			try{
				arquivo = new File(nomeDoArquivo);  
				diretorio = new File(arquivo.getPath());
				if(diretorio.exists()){
					System.out.println("Informe o titulo do ebook: ");
					titulo = entrada.nextLine();
					System.out.println("Informe a descri��o do ebook:");
					descricao = entrada.nextLine();
					System.out.println("Informe o g�nero:");
					genero = entrada.nextLine();
					do{
						entrada = new Scanner(System.in);
						System.out.println("Informe a quantidade de autores do ebook: ");// Pergunta ao usu�rio quantos autores ser�o cadastrados 
						try{
							qtdAutores = entrada.nextInt();
							entrada.nextLine();
							if(qtdAutores<=0){
								throw new IllegalArgumentException("N�mero de autores do Ebook deve ser pelo menos 1.");
							}
						}catch(IllegalArgumentException e){
							System.out.println(e.getMessage());
						}catch(InputMismatchException e){
							System.out.println("N�mero de autores do Ebook deve ser inteiro e positivo.");
							qtdAutores=0;
						}

					}while(qtdAutores<=0);
					do{	 
						System.out.println("Informe o autor: ");
						autor = entrada.nextLine();
						autores+="["+autor+"] ";
						qtdAutores--;
					}while(qtdAutores>1);
					System.out.println("Informe o local de produ��o do ebook:");
					local = entrada.nextLine();
					System.out.println("Informe a editora do ebook:");
					editora = entrada.nextLine();

					do{
						System.out.println("Informe o n�mero de p�ginas do ebook:");
						entrada = new Scanner(System.in);
						try{
							numeroDePaginas = entrada.nextInt();
							if(numeroDePaginas<=0)
								throw new IllegalArgumentException("N�mero de p�ginas deve ser pelo menos 1.");
							entrada.nextLine();
						}catch(IllegalArgumentException e){
							System.out.println(e.getMessage());
						}catch(InputMismatchException e){
							System.out.println("N�mero de p�ginas do Ebook deve ser inteiro e positivo.");
							numeroDePaginas=0;
						}
					}while(numeroDePaginas<=0);

					do{
						System.out.println("Informe o ano:");
						entrada = new Scanner(System.in);
						try{
							ano = entrada.nextInt();
							if(ano<=1950||ano>anoAtual)
								throw new IllegalArgumentException("Ano deve ser maior que 1950 e menor ou igual a "+anoAtual+".");
							entrada.nextLine();
						}catch(IllegalArgumentException e){
							System.out.println(e.getMessage());
						}catch(InputMismatchException f){
							System.out.println("Informe um ano v�lido(aaaa).");
							ano=0;
						}
					}while(ano<=1950||ano>anoAtual);
					cadastroEbook.cadastra(nomeDoArquivo,titulo,descricao,genero,autores,local,editora,numeroDePaginas,ano);
					System.out.println("O Ebook foi cadastrado com sucesso." );
				}
			}catch(NullPointerException e){
				System.out.println("Diret�rio informado n�o existe.");
			}	
		}
	}


	/**
	 * Metodo que exibe qual fun��o o usuario est� executando e invoca o metodo listarCadastros para executa-la.
	 */
	public void listarCadastros(){
		System.out.println("\n**************");
		System.out.println("3. EBOOK");
		System.out.println("2. LISTAR CADASTROS");
		System.out.println("**************");
		cadastroEbook.listarCadastros();
	}

	/**
	 * Este m�todo solicita ao usu�rio as informa��es sobre o ebook que ele deseja alterar.
	 */
	@SuppressWarnings("resource")
	public void editaEbook(){
		Scanner entrada = new Scanner(System.in);
		String titulo,tituloPAlterar;
		String autores = "";
		String nomeDoArquivo;
		String descricao;
		String genero;
		String autor;
		String local = null;
		String editora = null;
		Ebook existe = null;
		int numeroDePaginas = 0,qtdAutores = 0;
		int ano = 0;
		System.out.println("\n**************");
		System.out.println("3. EBOOK");
		System.out.println("3. ALTERAR");
		System.out.println("**************");
		System.out.println("Informe o t�tulo do Ebook ");
		tituloPAlterar = entrada.nextLine();
		existe = cadastroEbook.existe(tituloPAlterar);
		if(existe!=null){
			System.out.println("Informe o novo nome do arquivo do ebook: ");
			nomeDoArquivo = entrada.nextLine();
			System.out.println("Informe o novo titulo do ebook: ");
			titulo = entrada.nextLine();
			System.out.println("Informe a nova descri��o do ebook:");
			descricao = entrada.nextLine();
			System.out.println("Informe o novo g�nero:");
			genero = entrada.nextLine();
			System.out.println("Informe a nova quantidade de autores do ebook: ");// Pergunta ao usu�rio quantos autores ser�o cadastrados 
			do{
				entrada = new Scanner(System.in);
				System.out.println("Informe a quantidade de autores do ebook: ");// Pergunta ao usu�rio quantos autores ser�o cadastrados 
				try{
					qtdAutores = entrada.nextInt();
					entrada.nextLine();
					if(qtdAutores<=0){
						throw new IllegalArgumentException("N�mero de autores do Ebook deve ser pelo menos 1.");
					}
				}catch(IllegalArgumentException e){
					System.out.println(e.getMessage());
				}catch(InputMismatchException e){
					System.out.println("N�mero de autores do Ebook deve ser inteiro e positivo.");
					qtdAutores=0;
				}

			}while(qtdAutores<=0);
			do{	 
				System.out.println("Informe o autor: ");
				autor = entrada.nextLine();
				autores+="["+autor+"] ";
				qtdAutores--;
			}while(qtdAutores>1);
			System.out.println("Informe o local de produ��o do ebook:");
			local = entrada.nextLine();
			System.out.println("Informe a editora do ebook:");
			editora = entrada.nextLine();

			do{
				System.out.println("Informe o n�mero de p�ginas do ebook:");
				entrada = new Scanner(System.in);
				try{
					numeroDePaginas = entrada.nextInt();
					if(numeroDePaginas<=0)
						throw new IllegalArgumentException("N�mero de p�ginas deve ser pelo menos 1.");
					entrada.nextLine();
				}catch(IllegalArgumentException e){
					System.out.println(e.getMessage());
				}catch(InputMismatchException e){
					System.out.println("N�mero de p�ginas do Ebook deve ser inteiro e positivo.");
					numeroDePaginas=0;
				}
			}while(numeroDePaginas<=0);

			do{
				System.out.println("Informe o ano:");
				entrada = new Scanner(System.in);
				try{
					ano = entrada.nextInt();
					if(ano<=1950||ano>anoAtual)
						throw new IllegalArgumentException("Ano deve ser maior que 1950 e menor ou igual a "+anoAtual+".");
					entrada.nextLine();
				}catch(IllegalArgumentException e){
					System.out.println(e.getMessage());
				}catch(InputMismatchException f){
					System.out.println("Informe um ano v�lido(aaaa).");
					ano=0;
				}
			}while(ano<=1950||ano>anoAtual);
			cadastroEbook.edita(existe,nomeDoArquivo, titulo, descricao, genero,autores,local,editora,numeroDePaginas,ano);
			System.out.println("Ebook alterado com sucesso." );
		}
		else
			System.out.println("Ebook n�o encontrado." );
	}


	/**
	 * Este m�todo exclui um ebook informado pelo usu�rio.
	 */
	private void excluirEbook() {
		@SuppressWarnings("resource")
		Scanner entrada = new Scanner(System.in);
		String titulo = "";
		System.out.println("\n**************");
		System.out.println("3. EBOOK");
		System.out.println("5. EXCLUIR");
		System.out.println("**************");
		cadastroEbook.ordena(); 
		System.out.println("Informe o t�tulo do ebook ");
		titulo = entrada.nextLine();
		boolean confirma = cadastroEbook.exclui(titulo);
		if (confirma)
			System.out.println("Ebook excluido com Sucesso");
		else
			System.out.println("Ebook n�o excluido");
	}

	/**
	 * Metodo que exibe qual fun��o o usuario est� executando e invoca o metodo ordena para executa-la.
	 */
	private void ordenar() {
		System.out.println("\n**************");
		System.out.println("3. EBOOK");
		System.out.println("6. ORDENAR");
		System.out.println("**************");
		cadastroEbook.ordena(); 
		System.out.println("Lista ordenada.");
	}

	/**
	 * Metodo que exibe qual fun��o o usuario est� executando, exibe o menu para que o usuario escolha por qual atributo(titulo,ano,genero) deseja 
	 * pesquisar e invoca o respectivo metodo para executa-la.
	 */
	private void busca() {
		int numSel = 0;
		do{                       
			@SuppressWarnings("resource")
			Scanner sc = new Scanner(System.in);
			System.out.println("\n**************");
			System.out.println("3. EBOOK");
			System.out.println("4. BUSCAR");
			System.out.println("**************" + "\n"
					+ "\n"
					+ "\n   1. Buscar por T�tulo "
					+ "\n   2. Buscar por G�nero "
					+ "\n   3. Buscar por Ano "
					+ "\n   4. Voltar ao Menu ");
			try{
				numSel = sc.nextInt();
				switch (numSel) {
				case 1:
					buscarPorTitulo();  
					numSel = 6;
					break;                            
				case 2:
					buscarPorGenero();
					numSel = 6;
					break;
				case 3:
					buscarPorAno();
					numSel = 6;
					break;
				}
			}catch(InputMismatchException e){
				System.out.println("Informe um valor entre 1 e 4.");
			}
		}
		while (numSel != 4);
	}


	/**
	 * Este m�todo pede ao usu�rio o t�tulo para realizar outras opera��es, verifica se ele existe e retorna o t�tulo. 
	 */
	private void buscarPorTitulo() {
		@SuppressWarnings("resource")
		Scanner entrada = new Scanner(System.in);
		System.out.println("Informe o t�tulo do Ebook ");
		String titulo = entrada.nextLine();
		boolean buscado = false;
		try {
			buscado = cadastroEbook.consultaTitulo(titulo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(buscado==false)
			System.out.println("Ebook n�o cadastrado.");
	}

	/**
	 * Metodo que solicita ao usuario um genero e realiza a busca na LinkedList a procura de ebooks com aquele genero.
	 */
	private void buscarPorGenero() {
		@SuppressWarnings("resource")
		Scanner entrada = new Scanner(System.in);
		System.out.println("Informe o g�nero do Ebook ");
		String genero = entrada.nextLine();
		boolean find =false;
		try {
			find = cadastroEbook.consultaGenero(genero);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(find==false)
			System.out.println("N�o existe Ebook com este g�nero.");

	}

	/**
	 * Metodo que solicita ao usuario um ano e realiza a busca na LinkedList a procura de ebooks com aquele ano.
	 */
	private void buscarPorAno() {
		@SuppressWarnings("resource")
		Scanner entrada = new Scanner(System.in);
		System.out.println("Informe o ano do Ebook ");
		int ano;
		boolean find = false;
		try {
			ano = entrada.nextInt();
			find = cadastroEbook.consultaAno(ano);
		} catch (InputMismatchException e) {
			System.out.println("Informe um ano v�lido(aaaa).");
		}
		if(find==false)
			System.out.println("Ebook n�o cadastrado.");
	}

	/**
	 * Este m�todo exibe todos os ebooks armazenados.
	 * @param ebook
	 */
	public void imprimeEbook(Ebook ebook){
		System.out.println("\nNome : " + ebook.getNomeDoArquivo());
		System.out.println("Titulo : " + ebook.getTitulo());
		System.out.println("Descri��o : " + ebook.getDescricao());
		System.out.println("Genero : " + ebook.getGenero());
		System.out.println("Autores : " + ebook.getAutores());
		System.out.println("Local : " + ebook.getLocal());
		System.out.println("Editora : " + ebook.getEditora());
		System.out.println("N�mero de p�ginas : " + ebook.getNumeroDePaginas());
		System.out.println("Ano : " + ebook.getAno());
		System.out.println("-----------------------------------\n");
	}
}