package visao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import Excecoes.ExcecoesMidia;
import modelo.dados.ControlaFilme;
import modelo.entidade.Filme;

/**
 * 
 * @author Andr� Becker Classe Interface Filmes
 */
public class InterfaceFilmes {

	ControlaFilme cadastroFilme = new ControlaFilme();
	private Scanner entrada;

	public void cadastro() throws IOException {

		entrada = new Scanner(System.in);

		try {
			cadastroFilme.lerArquivo("Filme.bin");
		} catch (Exception e1) {

		}

		int opcao = 0;

		do {
			System.out.println("Menu Filme\n" + "1. Cadastrar Filme\n"
					+ "2. Exibir filmes cadastradas\n" + "3. Alterar filme\n"
					+ "4. Excluir filme\n"
					+ "5. Buscar por agrupamento de filmes\n." + "0. Sair.");

			opcao = entrada.nextInt();
			if (opcao == 1) {
				incluirFilme();
			} else if (opcao == 2) {

				listarFilme();

			} else if (opcao == 3) {
				alterarFilme();

			} else if (opcao == 4) {
				excluirFilme();
			} else if (opcao == 5) {
				try {
					cadastroFilme.criarArquivo("Filme.bin");
				} catch (IOException e) {

				}
				do {

					opcao = ExcecoesMidia.pedeInt("M�todos de busca: \n\n "
							+ "1. Buscar por Ano\n"
							+ "0. voltar  para o Menu de Filmes\n");

					String resposta;
					if (opcao == 1) {
						int res = ExcecoesMidia
								.pedeInt("Informe o Ano da filme que deseja listar: ");
						System.out.println(buscarPorAno(res));
					}
				} while (opcao != 0);
			}

		} while (opcao != 0);

	}

	/**
	 * Metodo que faz a inclusao de cada atributo
	 */
	public void incluirFilme() {

		Scanner entrada = new Scanner(System.in);

		String nomeDoArquivo;
		String titulo;
		String descricao;
		String genero;
		String idioma;
		String diretor = "";
		String atoresPrinc;
		ArrayList<String> atores = new ArrayList<String>();
		int duracao;
		int ano;
		String resposta;

		nomeDoArquivo = ExcecoesMidia.pedeString("Informe o nome da Filme:");

		titulo = ExcecoesMidia.pedeString("Informe o titulo da Filme:");

		descricao = ExcecoesMidia.pedeString("Informe a descri��o da Filme:");

		genero = ExcecoesMidia.pedeString("Informe o genero:");

		idioma = ExcecoesMidia.pedeString("Informe o idioma:");

		int i = 0;
		do {

			atoresPrinc = ExcecoesMidia.pedeString("Informe o nome do "
					+ (i + 1) + "� autor: ");

			atores.add(atoresPrinc);

			resposta = ExcecoesMidia
					.pedeString("Deseja adicionar outro ator ? S/N");
			i++;

		} while (resposta.equalsIgnoreCase("S"));

		duracao = ExcecoesMidia.pedeInt("Informe a dura��o: ");

		ano = ExcecoesMidia.pedeInt("Informe o ano: ");
		entrada.nextLine();

		Filme a = new Filme(nomeDoArquivo, titulo, descricao, genero, idioma,
				diretor, atoresPrinc, duracao, ano);

		if (cadastroFilme.incluirObjeto(a)) {
			System.out.println("Filme adicionada com sucesso ! \n");
		} else {
			System.out.println("Esse filme j� existe !");
		}
	}

	/**
	 * Metodo que altera o atributo que o usuario deseja
	 */
	public void alterarFilme() {

		Scanner entrada = new Scanner(System.in);

		String nomeDoArquivo;
		String titulo;
		String descricao;
		String genero;
		String idioma = "";
		String diretor;
		String atoresPrinc;
		int duracao;
		int ano;
		String resposta;
		
		System.out.println("Informe o titulo do filme a ser alterado: ");
		resposta = entrada.nextLine();
		if(cadastroFilme.existe(resposta)){
		nomeDoArquivo = ExcecoesMidia
				.pedeString("Informe o novo nome da Filme:");

		titulo = ExcecoesMidia.pedeString("Informe o novo titulo da Filme:");

		descricao = ExcecoesMidia
				.pedeString("Informe a nova descri��o da Filme:");

		genero = ExcecoesMidia.pedeString("Informe o novo genero:");

		idioma = ExcecoesMidia.pedeString("Informe o novo idioma:");

		diretor = ExcecoesMidia.pedeString("Informe o novo diretor:");

		atoresPrinc = ExcecoesMidia.pedeString("Informe o novo atore");

		duracao = ExcecoesMidia.pedeInt("Informe a duracao:");

		ano = ExcecoesMidia.pedeInt("Informe o novo ano:");

		Filme f = new Filme(nomeDoArquivo, titulo, descricao, genero, idioma, diretor, atoresPrinc, duracao, ano);

		cadastroFilme.alterar(resposta, f);
	}else{
		
		System.out.println("Filme n�o existe ! ");
	}
		

	}

	/**
	 * Metodo que exclui pelo paremetro titulo
	 * 
	 * @param titulo
	 */
	public void excluirFilme() {
		Scanner sc = new Scanner(System.in);
		String titulo;

		titulo = ExcecoesMidia
				.pedeString("Informe o titulo do filme que deseja excluir: ");

		if (cadastroFilme.excluir(titulo) != null) {
			System.out.print("Filme removido com sucesso");
		} else {
			System.out.print("Filme nao encontrado");
		}
	}

	/**
	 * Metodo que lista todos filmes cadastrados
	 */
	public void listarFilme() {
		entrada = new Scanner(System.in);
		String resposta;

		if (cadastroFilme.listarCadastros() != null) {
			System.out.println(cadastroFilme.listarCadastros().toString());

			System.out.println("\n\n");
		} else {
			System.out.println("nenhum filme encontrada !");
			System.out.println("Deseja cadastrar um filme ? S/N");
			resposta = entrada.nextLine();
			if (resposta.equalsIgnoreCase("s")) {

				incluirFilme();
			}
		}
	}

	/**
	 * Metodo que busca pelo titulo do filme
	 * 
	 * @param titulo
	 * @return titulo
	 */
	public int buscarPorAno(int ano) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Informe o titulo do Filme a ser alterado");
		ano = sc.nextInt();
		cadastroFilme.buscaPorAno(ano);

		return ano;
	}

	/**
	 * Metodo que imprime filmes.
	 * 
	 * @param a
	 */
	// public void imprimeFilmes(Filme a) {

	// System.out.println("Nome Do Arquivo: " + a.getNomeDoArquivo());
	// System.out.println("Titulo: \n" + a.getTitulo());
	// System.out.println("Descricao: " + a.getDescricao());
	// System.out.println("Genero: " + a.getGenero());
	// System.out.println("Idioma: " + a.getIdioma());
	// System.out.println("Diretor: " + a.getDiretor());
	// System.out.println("Atores Principais: " + a.getAtoresPrinc());
	// System.out.println("Duracao: " + a.getDuracao());
	// System.out.println("Ano: " + a.getAno());

	// }
}