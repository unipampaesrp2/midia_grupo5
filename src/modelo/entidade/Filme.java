package modelo.entidade;

public class Filme extends Midia {

	private String idioma;
	private String diretor;
	private String atoresPrinc;
	private int duracao;
	private int ano;

	public Filme(String nomeDoArquivo, String titulo, String descricao, String genero, String idioma, String diretor, String atoresPrinc, int duracao, int ano){
		super(nomeDoArquivo, titulo, descricao, genero);
		this.idioma = idioma;
		this.diretor = diretor;
		this.atoresPrinc = atoresPrinc;
		this.duracao = duracao;
		this.ano = ano;
		
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public String getDiretor() {
		return diretor;
	}

	public void setDiretor(String diretor) {
		this.diretor = diretor;
	}

	public String getAtoresPrinc() {
		return atoresPrinc;
	}

	public void setAtoresPrinc(String atoresPrinc) {
		this.atoresPrinc = atoresPrinc;
	}

	public int getDuracao() {
		return duracao;
	}

	public void setDuracao(Integer duracao) {
		this.duracao = duracao;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}
	
	public String toString(){
		String str = "nomeDoArquivo: " + nomeDoArquivo +"\n";
		str += "T�tulo: " + titulo + "\n";
		str += "Descri��o: " + descricao +"\n";
		str +=  "G�nero: " + genero + "\n";
		str += "Idioma: " + idioma + "\n";
		str+=  "diretor: " + diretor+ "\n";
		str += "Atores Principais: " + atoresPrinc + "\n";
		str += "Dura��o: " + duracao + "\n";
		str += "Ano: " + ano + "\n";
		return str;
		
		/*
		 * diretor = diretor;
		this.atoresPrinc = atoresPrinc;
		this.duracao = duracao;
		this.ano = ano;
		 */
		
	}

}
