package modelo.entidade;

/**
 * 
 * @author Mario
 * A classe Ebook extende a classe midia, implementado os atributos espec�ficos do ebook.
 */
@SuppressWarnings("serial")
public class Ebook extends Midia {
	private String autores;//Cole��o de autores de cada ebook.
	private String local;//Local de produ��o do ebook.
	private String editora;//Editora que publicou o ebook.
	private int numeroDePaginas;//N�mero de p�ginas do ebook.
	private int ano;//Ano de produ��o do ebook.
	
	/**
	 * Construtor do ebook.
	 * @param nomeDoArquivo Endere�o da mem�ria onde est� o ebook.
	 * @param titulo
	 * @param descricao
	 * @param genero
	 * @param autores
	 * @param local
	 * @param editora
	 * @param numeroDePaginas
	 * @param ano
	 */
	public Ebook(String nomeDoArquivo, String titulo, String descricao, String genero,String autores, String local, String editora, 
			int numeroDePaginas, int ano) {
		super(nomeDoArquivo, titulo, descricao, genero);
		this.autores=autores;
		this.local=local;
		this.editora=editora;
		this.numeroDePaginas=numeroDePaginas;
		this.ano=ano;
	}
	
	/**
	 * Metodo que retorna os autores do ebook.
	 * @return
	 */
	public String getAutores() {
		return autores;
	}
	
	/**
	 * Metodo que recebe os autores por parametro e substitui a referencia anterior.
	 * @param autores
	 */
	public void setAutores(String autores) {
		this.autores = autores;
	}
	
	/**
	 * Metodo que retorna o local de produ��o do ebook.
	 * @return
	 */
	public String getLocal() {
		return local;
	}

	/**
	 * Metodo que recebe um local por parametro e o atribui a variavel local do ebook.
	 * @param local
	 */
	public void setLocal(String local) {
		this.local = local;
	}

	/**
	 * Metodo que retorna a editora do ebook.
	 * @return 
	 */
	public String getEditora() {
		return editora;
	}

	/**
	 * Metodo que recebe uma editora por parametro e a atribui a variavel editora do ebook.
	 * @param editora
	 */
	public void setEditora(String editora) {
		this.editora = editora;
	}

	/**
	 * Metodo que retorna o numero de paginas do ebook.
	 * @return
	 */
	public int getNumeroDePaginas() {
		return numeroDePaginas;
	}

	/**
	 * Metodo que recebe o numero de paginas por parametro e o atribui a variavel numeroDePaginas do ebook.
	 * @param numeroDePaginas
	 */
	public void setNumeroDePaginas(int numeroDePaginas) {
		this.numeroDePaginas = numeroDePaginas;
	}

	/**
	 * Metodo que retorna o ano de produ��o do ebook.
	 * @return
	 */
	public int getAno() {
		return ano;
	}

	/**
	 * Metodo que recebe uma variavel int ano por parametro e a atribui a variavel ano do ebook.
	 * @param ano
	 */
	public void setAno(int ano) {
		this.ano = ano;
	}
}