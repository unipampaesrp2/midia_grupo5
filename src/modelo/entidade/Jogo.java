package modelo.entidade;

/**
 * 
 */
import java.util.ArrayList;

/**
 * 
 * @author eduardo
 * 
 */
public class Jogo extends Midia implements Comparable<Jogo>{
	/**
 * 
 */
	private static final long serialVersionUID = 1L;
	private ArrayList<String> autores;
	private int ano;
	private int nJogadores;
	private boolean sRede;

	/**
	 * 
	 * @param nomeDoArquivo
	 * @param titulo
	 * @param descricao
	 * @param genero
	 * @param autores
	 * @param ano
	 * @param nJogadores
	 * @param sRede
	 */
	public Jogo(String nomeDoArquivo, String titulo, String descricao,
			String genero, ArrayList<String> autores, int ano, int nJogadores,
			boolean sRede) {
		super(nomeDoArquivo, titulo, descricao, genero);
		this.autores = autores;
		this.ano = ano;
		this.nJogadores = nJogadores;
		this.sRede = sRede;
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<String> getAutores() {
		return autores;
	}

	/**
	 * 
	 * @param autores
	 */
	public void setAutores(ArrayList<String> autores) {
		this.autores = autores;
	}

	/**
	 * 
	 * @return
	 */
	public int getAno() {
		return ano;
	}

	/**
	 * 
	 * @param ano
	 */
	public void setAno(int ano) {
		this.ano = ano;
	}

	/**
	 * 
	 * @return
	 */
	public int getnJogadores() {
		return nJogadores;
	}

	/**
	 * 
	 * @param nJogadores
	 */
	public void setnJogadores(int nJogadores) {
		this.nJogadores = nJogadores;
	}

	/**
	 * 
	 * @return
	 */
	public boolean issRede() {
		return sRede;
	}

	/**
	 * 
	 * @param sRede
	 */
	public void setsRede(boolean sRede) {
		this.sRede = sRede;
	}

	/**
	 * 
	 * @return
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
 * 
 */
	@Override
	public String toString() {
		return "\n nomeDoArquivo" + getNomeDoArquivo() + "\n Titulo: "
				+ getTitulo() + "\n Descricao: " + getDescricao()
				+ "\n Genero: " + getGenero() + "\n Autores: " + getAutores()
				+ "\n Ano: " + getAno() + "\n Nº Jogadores: " + getnJogadores()
				+ "\n Rede: " + issRede() + "\n";

	}
/**
 * 
 */
	public int compareTo(Jogo o) {
				Jogo e = (Jogo) o;
				if (this.ano > e.getAno())
					return 1;
				if (this.ano < e.getAno())
					return -1;
				if (this.ano == e.getAno())
					return 0;
				return 0;
	}


}