package modelo.entidade;

import java.io.Serializable;

public abstract class Midia implements Serializable {
		

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nomeDoArquivo == null) ? 0 : nomeDoArquivo.hashCode());
		result = prime * result + ((titulo == null) ? 0 : titulo.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Midia other = (Midia) obj;
		if (nomeDoArquivo == null) {
			if (other.nomeDoArquivo != null)
				return false;
		} else if (!nomeDoArquivo.equals(other.nomeDoArquivo))
			return false;
		if (titulo == null) {
			if (other.titulo != null)
				return false;
		} else if (!titulo.equals(other.titulo))
			return false;
		return true;
	}


	protected String nomeDoArquivo;
	protected String titulo;
	protected String descricao;
	protected String genero;
	
	
	public Midia(String nomeDoArquivo, String titulo, String descricao,String genero) {		
		this.nomeDoArquivo = nomeDoArquivo;
		this.titulo = titulo;
		this.descricao = descricao;
		this.genero = genero;
	}


	public String getNomeDoArquivo() {
		return nomeDoArquivo;
	}


	public void setNomeDoArquivo(String nomeDoArquivo) {
		this.nomeDoArquivo = nomeDoArquivo;
	}


	public String getTitulo() {
		return titulo;
	}


	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}


	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public String getGenero() {
		return genero;
	}


	public void setGenero(String genero) {
		this.genero = genero;
	}

	
	
}
	