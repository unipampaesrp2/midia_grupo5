package modelo.entidade;

import java.util.ArrayList;
/**
 * Classe Musica com os atributos e classe e os atributos herdados da classe Abstrata Midia
 * @author MatheusLe�o
 *
 */

public class Musica extends Midia{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private String idioma;
	private ArrayList<String> autores;
	private ArrayList<String> interpretes;
	private int duracao;
	private int ano;

	/**
	 * Construtor da classe Musica 
	 * @param nomeDoArquivo
	 * @param titulo
	 * @param descricao
	 * @param genero
	 * @param idioma
	 * @param autores
	 * @param interpretes
	 * @param duracao
	 * @param ano
	 */
	public Musica(String nomeDoArquivo, String titulo, String descricao,
			String genero, String idioma, ArrayList<String> autores, ArrayList<String> interpretes,
			int duracao, int ano) {
		super(nomeDoArquivo, titulo, descricao, genero);
		this.idioma = idioma;
		this.autores = autores;
		this.interpretes = interpretes;
		this.duracao = duracao;
		this.ano = ano;
	}

	/**
	 * Getters e Setters dos atributos
	 */

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public ArrayList<String> getAutores() {
		return autores;
	}

	public void setAutores(ArrayList<String> autores) {
		this.autores = autores;
	}

	public ArrayList<String> getInterpretes() {
		return interpretes;
	}

	public void setInterpretes(ArrayList<String> interpretes) {
		this.interpretes = interpretes;
	}

	public int getDuracao() {
		return duracao;
	}

	public void setDuracao(int duracao) {
		this.duracao = duracao;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}
	
	@Override
	public String toString() {
		return 	"nomeDoArquivo:" + nomeDoArquivo
				+ "\n Autores:" + autores  
				+ "\n Interpretes: " + interpretes 
				+ "\n Durac�o: " + duracao 
				+ "\n ano=" + ano 
				+ "\n Idioma: " + idioma 
				+ "\n T�tulo: " + titulo 
				+ "\n Descricao: " + descricao 
				+ "\n G�nero: " + genero;
	}


}