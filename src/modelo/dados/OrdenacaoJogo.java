package modelo.dados;



import java.util.*;

import modelo.entidade.Jogo;

//QuickSort
public class OrdenacaoJogo<T extends Comparable<T>> {

	private T[] a;

	private Jogo[] getVetorDeJogo(Vector<Jogo> jogos) {
		Jogo[] vectorJogos = new Jogo[jogos.size()];
		for (int count = 0; count < jogos.size(); count++) {
			vectorJogos[count] = jogos.get(count);
		}
		return vectorJogos;
	}

	public Vector<Jogo> sort(Vector<Jogo> jogos) {
		a = (T[]) getVetorDeJogo(jogos);
		int left = 0;

		int right = a.length - 1;

		quickSort(left, right);

		Vector<Jogo> ordenados = new Vector<Jogo>();
		for (Jogo temp : (Jogo[]) a) {
			ordenados.add(temp);
		}
		return ordenados;
	}

	// Este m�todo � usado para classificar a matriz usando o algoritmo
	// quicksort .

	// Leva � esquerda e a extremidade direita da matriz como dois cursores

	private void quickSort(int esquerda, int direita) {

		// Se ambos cursor esquadrinhou os completos sa�das matriz quicksort

		if (esquerda >= direita)

			return;

		// Pivot usando mediana de 3 abordagem

		T pivot = getMedian(esquerda, direita);

		int partition = partition(esquerda, direita, pivot);

		// Recursivamente , chama o quicksort com os diferentes par�metros
		// esquerdo e direito de sub - matriz

		quickSort(0, partition - 1);

		quickSort(partition + 1, direita);

	}

	// Este m�todo � utilizado para dividir a matriz dado e retorna o inteiro
	// que indica o �ndice de piv� ordenada

	private int partition(int esquerda, int direita, T pivot) {

		int esquerdaCursor = esquerda - 1;

		int direitaCursor = direita;

		while (esquerdaCursor < direitaCursor) {

			while (((Comparable<T>) a[++esquerdaCursor]).compareTo(pivot) < 0)
				;

			while (direitaCursor > 0
					&& ((Comparable<T>) a[--direitaCursor]).compareTo(pivot) > 0)
				;

			if (esquerdaCursor >= direitaCursor) {

				break;

			} else {

				swap(esquerdaCursor, direitaCursor);

			}

		}

		swap(esquerdaCursor, direita);

		return esquerdaCursor;

	}

	public T getMedian(int esquerda, int direita) {

		int center = (esquerda + direita) / 2;

		if (((Comparable<T>) a[esquerda]).compareTo(a[center]) > 0)

			swap(esquerda, center);

		if (((Comparable<T>) a[esquerda]).compareTo(a[direita]) > 0)

			swap(esquerda, direita);

		if (((Comparable<T>) a[center]).compareTo(a[esquerda]) > 0)

			swap(center, direita);

		swap(center, direita);

		return a[direita];

	}

	// Este m�todo � usado para trocar os valores entre os dois dado �ndice

	public void swap(int left, int right) {

		T temp = a[left];

		a[left] = a[right];

		a[right] = temp;

	}
}
