package modelo.dados;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.*;

import modelo.entidade.Musica;

public class OrdenacaoEBuscaMusica {

	List<Musica> ordena = new ArrayList<Musica>();

	public void lerArquivo(String nomeArquivo) throws FileNotFoundException, IOException, ClassNotFoundException {

		ObjectInputStream obj;
		obj = new ObjectInputStream(new FileInputStream(new File(nomeArquivo)));

		Set<Musica> readObject = (HashSet<Musica>) obj.readObject();
		ordena.addAll(readObject);
		obj.close();

	}

	public void ordenarAno() {

		int i = 0, j;
		Musica eleito;
		for (Musica m : ordena) {

			eleito = m;
			j = i - 1;

			while ((j >= 0) && (eleito.getAno() < ordena.get(j).getAno())) {
				ordena.set(j + 1, ordena.get(j));
				j--;
			}

			ordena.set(j + 1, eleito);
		}

	}

	/**
	 * M�todo para ordenar o ArrayList por T�tulo
	 */
	public void ordenarTitulo() {
		int i = 0, j; // vari�vel utilizada no for e while
		Musica eleito; // vari�vel usada para guardar temporiariamente o valor
						// do elemento
		for (Musica m : ordena) { // la�o para percorrer o
									// arrayList de objetos para
									// compara��o
			eleito = m; // a vari�vel eleito recebe a primeira musica do array
			j = i - 1; // a vari�vel J recebe o valor de i-1

			while ((j >= 0) && (eleito.getTitulo().compareToIgnoreCase(ordena.get(j).getTitulo()) < 0)) {
				ordena.set(j + 1, ordena.get(j));
				j--;
			}
			ordena.set(j + 1, eleito); // j vai ser incrementado e
		}
	}

	public String buscaPorGenero(String genero) {
		String str;
		for (int i = 0; i < ordena.size(); i++) {
			if (ordena.get(i).getGenero().equals(genero)) {
				str = "nomeDoArquivo:" + ordena.get(i).getNomeDoArquivo() + "\n Autores:" + ordena.get(i).getAutores()
						+ "\n Interpretes: " + ordena.get(i).getInterpretes() + "\n Durac�o: "
						+ ordena.get(i).getDuracao() + "\n ano: " + ordena.get(i).getAno() + "\n Idioma: "
						+ ordena.get(i).getIdioma() + "\n T�tulo: " + ordena.get(i).getTitulo() + "\n Descricao: "
						+ ordena.get(i).getDescricao() + "\n G�nero: " + ordena.get(i).getGenero();
				return str;
			}

		}
		return null;
	}

	public String buscaPorAno(int ano) {
		String str;

		for (int i = 0; i < ordena.size(); i++) {
			if (ordena.get(i).getAno() == ano) {

				str = ordena.toString();
				// str = "nomeDoArquivo:" + ordena.get(i).getNomeDoArquivo() +
				// "\n Autores:" + ordena.get(i).getAutores()
				// + "\n Interpretes: " + ordena.get(i).getInterpretes() +
				// "\n Durac�o: "
				// + ordena.get(i).getDuracao() + "\n ano: " +
				// ordena.get(i).getAno() + "\n Idioma: "
				// + ordena.get(i).getIdioma() + "\n T�tulo: " +
				// ordena.get(i).getTitulo() + "\n Descricao: "
				// + ordena.get(i).getDescricao() + "\n G�nero: " +
				// ordena.get(i).getGenero();
				return str;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "OrdenacaoEBuscaMusica [ordena=" + ordena + "]";
	}

	public String buscaPorAutor(String autor) {
		String str;
		for (int i = 0; i < ordena.size(); i++) {

			if (ordena.get(i).getAutores().equals(autor)) {
				str = "nomeDoArquivo:" + ordena.get(i).getNomeDoArquivo() + "\n Autores:" + ordena.get(i).getAutores()
						+ "\n Interpretes: " + ordena.get(i).getInterpretes() + "\n Durac�o: "
						+ ordena.get(i).getDuracao() + "\n ano: " + ordena.get(i).getAno() + "\n Idioma: "
						+ ordena.get(i).getIdioma() + "\n T�tulo: " + ordena.get(i).getTitulo() + "\n Descricao: "
						+ ordena.get(i).getDescricao() + "\n G�nero: " + ordena.get(i).getGenero();
				return str;
			}

		}
		return null;
	}
}
