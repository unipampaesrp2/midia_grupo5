package modelo.dados;
/**
 * Classe que padroniza as assinaturas dos metodos de ordena��o e busca para as diferentes midias.
 * @author Mario
 *
 */
public interface InterfaceOrdenacaoBusca {
	
	/**
	 *Metodo que ira ordenar as diferentes cole��es. 
	 */
	public void ordena();
	
	/**
	 * Metodo que realizar� a consulta recebendo o titulo por parametro.
	 * @return true caso exista algum ebook com o atributo titulo igual ao informado por parametro.
	 */
	public boolean consultaTitulo(String titulo);
	
	/**
	 * Metodo que realizar� a consulta recebendo o ano por parametro.
	 * @return true caso exista algum ebook com o atributo ano igual ao informado por parametro.
	 */
	public boolean consultaAno(int ano);
	
	/**
	 * Metodo que realizar� a consulta recebendo o genero por parametro.
	 * @return true caso exista algum ebook com o atributo genero igual ao informado por parametro.
	 */
	public boolean consultaGenero(String genero);
}
