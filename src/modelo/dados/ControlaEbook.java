
package modelo.dados;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.List;

import visao.InterfaceEbook;
import modelo.entidade.Ebook;

/**
 * 
 * @author Mario
 *Esta classe realiza todo o processamento da classe Ebook.
 */
public class ControlaEbook implements InterfaceOrdenacaoBusca,InterfaceLeGrava{
	@SuppressWarnings("unused")
	private File databaseFile;
	private LinkedList<Ebook> ebookArmazenados;
	
	/**
	 * Construtor da classe ControlaEbook.
	 */
	public ControlaEbook(){
		ebookArmazenados=new LinkedList<Ebook>();
	}
	
	/**
	 * Metodo que retorna a List de ebooks.
	 * @return
	 */
	public List<Ebook> getEbooksArmazenados() {
		return ebookArmazenados;
	}

	/**
	 * Metodo que seta a List de ebooks
	 * @param ebookArmazenados
	 */
	public void setEbooksArmazenados(List<Ebook> ebookArmazenados) {
		this.ebookArmazenados = (LinkedList<Ebook>) ebookArmazenados;
	}
	
	/**
	 * Este m�todo inclui o novo ebook no ArrayList de Ebooks, recebendo por parametro os atributos dele.
	 * @param nomeDoArquivo
	 * @param titulo
	 * @param descricao
	 * @param genero
	 * @param autores
	 * @param local
	 * @param editora
	 * @param numeroDePaginas
	 * @param ano
	 */
	public void cadastra(String nomeDoArquivo, String titulo, String descricao,String genero, String autores, String local, String editora,
			int numeroDePaginas, int ano) {
		Ebook novoEbook = new Ebook(nomeDoArquivo, titulo, descricao, genero, autores,local,editora,numeroDePaginas,ano);
		getEbooksArmazenados().add(getSortedIndex(titulo,ebookArmazenados),novoEbook);//Adiciona o novoEbook ao ArrayList <ebooksArmazenados>.
	}

	/**
	 * Metodo que verifica a posi��o em que o novo ebook serah incluido utilizando de um insertion sort e inserindo em ordem alfabetica.
	 * @param titulo do ebook
	 * @param list Lista de objetos da classe Ebook.
	 * @return a posi��o em que o ebook deve ser inserido caso seja menor que o ultimo e o size da list caso n�o seja.
	 */
	public int getSortedIndex(String titulo,List<Ebook> list){
		 for (int i=0; i < list.size(); i++) {
		        if (titulo.compareTo(((Ebook) list.get(i)).getTitulo()) < 0) {
		            return i;
		        }
		    }       
		    return list.size();
	}
	
	/**
	 * Metodo que recebe por parametro o titulo do ebook e verifica se na List existe algum ebook com o titulo informado.
	 * @param titulo
	 * @return caso exista retorna a posi��o do ebook e caso contrario retorna null.
	 */
	public Ebook existe(String titulo){
		for (int pos = 0; pos < ebookArmazenados.size(); pos++) {
			if (ebookArmazenados.get(pos).getTitulo().equalsIgnoreCase(titulo)) {
				 return ebookArmazenados.get(pos);
			}
		}
		return null;
	}
	
	/**
	 * Metodo que recebe por parametro o endere�o do ebook e verifica se na List existe algum ebook com o endere�o informado.
	 * @param nomeDoArquivo
	 * @return True caso exista e caso contrario retorna false.
	 */
	public boolean existeFile(String nomeDoArquivo){
		for (int pos = 0; pos < ebookArmazenados.size(); pos++) {
			if (ebookArmazenados.get(pos).getNomeDoArquivo().equalsIgnoreCase(nomeDoArquivo)) {
				 return true;
			}
		}
		return false;
	}
	
	/**
	 * Este m�todo altera o ebook informado pelo usu�rio, atrav�s do atributo titulo. Recebendo por parametro os novos atributos dele.
	 * @param existe 
	 * @param nomeDoArquivo
	 * @param titulo
	 * @param descricao
	 * @param genero
	 * @param autores
	 * @param local
	 * @param editora
	 * @param numeroDePaginas
	 * @param ano
	 * @throws Exception 
	 */
	public void edita(Ebook existe, String nomeDoArquivo, String titulo,String descricao, String genero, String autores, String local,
			String editora, int numeroDePaginas, int ano) {	
		existe.setNomeDoArquivo(nomeDoArquivo);
		existe.setTitulo(titulo);
		existe.setDescricao(descricao);
		existe.setGenero(genero);
		existe.setAutores(autores);
		existe.setLocal(local);
		existe.setEditora(editora);
		existe.setNumeroDePaginas(numeroDePaginas);
		existe.setAno(ano);
	}

	/**
	 * Este m�todo exclui o ebook informado pelo usu�rio, atrav�s do atributo titulo. Recebendo por parametro o t�tulo dele.
	 * @param titulo
	 * @return
	 */
	public boolean exclui(String titulo){
		for (int pos = 0; pos < ebookArmazenados.size(); pos++) {
			if (ebookArmazenados.get(pos).getTitulo().equalsIgnoreCase(titulo)) {
				ebookArmazenados.remove(pos);
				return true;
			}
		}
		return false;
	}

	/**
	 * Este m�todo realiza a ordena��o do ArrayList <ebooksArmazenados> utilizando o m�todo selection sort.
	 */
	public void ordena/*selection*/() {
		for (int fixo = 0; fixo < ebookArmazenados.size()-1; fixo++) {/*Percorre o ArrayList <ebookArmazenados> ate a pen�ltima posi��o para compar�-lo
		at� a �ltima posi��o atrav�s do for abaixo.*/
			int menor = fixo;//A vari�vel menor recebe o ebook de menor posi��o come�ando pela primeira posi��o do ArrayList.
			for (int i = fixo + 1; i < ebookArmazenados.size(); i++){
				/*Compara-se o t�tulo do ebook da menor posi��o(menor) com o ebook da posi��o (i) usando o m�todo compareTo.
				  Esse m�todo pode retornar 0 se as strings forem iguais, um n�mero negativo se a string que invoca o compareTo for menor que a string
				   que � passada como um argumento e um n�mero positivo se a string que invoca o compareTo for maior que a string que � passada como 
				   argumento.
				 */
				if (ebookArmazenados.get(i).getTitulo().compareTo(ebookArmazenados.get(menor).getTitulo())<0){
					menor = i;// A variavel � trocada pela posi��o do ebook que tem o t�tulo menor. 
				}
			}
			if (menor != fixo) {
				// Troca
				Ebook t = ebookArmazenados.get(fixo);/*� criado uma vari�vel auxiliar que recebe a posi��o fixo do ArrayList e � usado os set para 
				realizar as trocas de posi��o no ArrayList
				*/
				ebookArmazenados.set(fixo,ebookArmazenados.get(menor));
				ebookArmazenados.set(menor, t);
			}
		}
	}

	/**
	 * Este m�todo verifica se o ebook de t�tulo informado pelo usu�rio existe na List e invoca o m�todo de sa�da.  
	 * @param titulo
	 * @return True caso exista algum ebook e false caso contrario.
	 */
	public boolean consultaTitulo(String titulo){
		boolean find=false;
		InterfaceEbook interEbook=new InterfaceEbook();
		for (int pos = 0; pos < ebookArmazenados.size(); pos++) {
			if (ebookArmazenados.get(pos).getTitulo().equalsIgnoreCase(titulo)) {
				 interEbook.imprimeEbook(ebookArmazenados.get(pos));
				 find = true;
			}
		}
		return find;
	}
	
	/**
	 *Este m�todo verifica se o ebook de g�nero informado pelo usu�rio existe na List e invoca o m�todo de sa�da.  
	 * @param titulo
	 * @return True caso exista algum ebook e false caso contrario.
	 */
	public boolean consultaGenero(String genero){
		boolean find=false;
		InterfaceEbook interEbook=new InterfaceEbook();
		for (int pos = 0; pos < ebookArmazenados.size(); pos++) {
			if (ebookArmazenados.get(pos).getGenero().equalsIgnoreCase(genero)) {
				interEbook.imprimeEbook(ebookArmazenados.get(pos));		
				find = true;				
			}
		}
		return find;
	}

	/**
	 *Este m�todo verifica se o ebook de ano informado pelo usu�rio existe na List e invoca o m�todo de sa�da.  
	 * @param titulo
	 * @return True caso exista algum ebook e false caso contrario.
	 */
	public boolean consultaAno(int ano){
		boolean find=false;
		InterfaceEbook interEbook=new InterfaceEbook();
		for (int pos = 0; pos < ebookArmazenados.size(); pos++) {
			if (ebookArmazenados.get(pos).getAno()==ano) {
				interEbook.imprimeEbook(ebookArmazenados.get(pos));		
				find = true;				
			}
		}
		return find;
	}	
	
	/**
	 * Este m�todo percorre todo ArrayList para exibir todos os objetos pelo m�todo de sa�da. 
	 */
	public void listarCadastros() {
		for (int i = 0; i < ebookArmazenados.size(); i++) {
			
			Ebook ebooks = getEbooksArmazenados().get(i);
			InterfaceEbook interEbook=new InterfaceEbook();
			interEbook.imprimeEbook(ebooks);
		}
	}

	/**
	 * Este m�todo l� as informa��o no arquivo binario.
	 * @param nomeArquivo 
	 * @param contr
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	public void lerArquivo(String nomeArquivo) throws IOException,  FileNotFoundException, ClassNotFoundException{
		
		ObjectInputStream obj;

		obj = new ObjectInputStream(new FileInputStream(new File(nomeArquivo)));

		@SuppressWarnings("unchecked")
		LinkedList<Ebook> readObject = (LinkedList<Ebook>) obj.readObject();
		ebookArmazenados = readObject;

		obj.close();
	}
	
	
	/**
	 * Este m�todo grava as informa��es no arquivo binario. 
	 * @throws IOException
	 */
	public void criarArquivo() throws IOException{
		ObjectOutputStream obj;

		obj = new ObjectOutputStream(new FileOutputStream(new File("ebook.txt")));

		obj.writeObject(ebookArmazenados);

		obj.close();
	}
}