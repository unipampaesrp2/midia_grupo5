package modelo.dados;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface InterfaceLeGrava {

	public void lerArquivo(String nomeArquivo) throws IOException, FileNotFoundException, ClassNotFoundException;
	
	public void criarArquivo() throws IOException;
}
