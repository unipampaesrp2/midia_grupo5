package modelo.dados;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;

import visao.InterfaceFilmes;
import modelo.entidade.Filme;
import modelo.entidade.Musica;

public class ControlaFilme {
	private ArrayList<Filme> filmesArmazenados;

	/**
	 * Construtor de ArrayList Filmes
	 * 
	 * @return
	 */
	public ControlaFilme() {

		filmesArmazenados = new ArrayList<Filme>();
	}

	/**
	 * 
	 * @return filmesArmazenados.
	 */
	public ArrayList<Filme> getfilmesArmazenados() {
		return filmesArmazenados;
	}

	/**
	 * 
	 * @param filmesArmazenados
	 */
	public void setFilmesArmazenados(ArrayList<Filme> filmesArmazenados) {
		this.filmesArmazenados = filmesArmazenados;
	}

	/**
	 * M�todo de inclusao que salva no arrayList filmesArmazenados.
	 * 
	 * @param nomeDoArquivo
	 * @param titulo
	 * @param descricao
	 * @param genero
	 * @param idioma
	 * @param diretor
	 * @param atoresPrinc
	 * @param duracao
	 * @param ano
	 */
	public void incluir(String nomeDoArquivo, String titulo, String descricao,
			String genero, String idioma, String diretor, String atoresPrinc,
			int duracao, int ano) {

		Filme a = new Filme(nomeDoArquivo, titulo, descricao, genero, idioma,
				diretor, atoresPrinc, duracao, ano);

		filmesArmazenados.add(a);

	}

	/*
	 * Metodo que inclui objeto no ArrayList
	 * 
	 * @param a
	 */
	public boolean incluirObjeto(Filme a) {

		if(filmesArmazenados.add(a)){
			return true;
		}
		
	return false;

	}

	public boolean existe(String titulo) {
		for (Filme filme : filmesArmazenados) {

			if (filme.getTitulo().equalsIgnoreCase(titulo)) {

				return true;
			}

		}
		return false;

	}

	/**
	 * Metodo que altera os atributos pelo objeto
	 */
	public boolean alterar(String titulo, Filme f) {
		for (int i = 0; i < filmesArmazenados.size(); i++) {
			if (filmesArmazenados.get(i).getTitulo().equalsIgnoreCase(titulo)) {

				filmesArmazenados.set(i, f);
				return true;
			}
		}
		return false;
		
		

	}

	/**
	 * Metodo que procura a posicao no arrayList e remove por parametro titulo e
	 * se nao achar retorna.
	 * 
	 * @return null
	 */
	public Filme excluir(String titulo) {
		for (int pos = 0; pos < getfilmesArmazenados().size(); pos++) {
			Filme a = getfilmesArmazenados().get(pos);
			if (a.getTitulo().equalsIgnoreCase(titulo)) {
				getfilmesArmazenados().remove(a);
			}
		}
		return null;
	}

	/**
	 * Metodo que exibe os filmes cadastrados no arrayList
	 * 
	 * @return null
	 */
	public String listarCadastros() {
		String f = "";
		for (int i = 0; i < filmesArmazenados.size(); i++) {
			f+= (i+1)+" Filme: \n";
			f += filmesArmazenados.get(i).toString();
			f+= "\n";

		}
		return f;
	}

	/*
	 * Metodo que percorre o arrayList e ordena por titulo
	 */
	public void ordenarPorTitulo() {
		Filme aux;
		for (int i = 0; i < filmesArmazenados.size(); i++) {
			for (int j = 0; j < filmesArmazenados.size() - 1; j++) {
				if (filmesArmazenados
						.get(j)
						.getTitulo()
						.compareToIgnoreCase(
								filmesArmazenados.get(j + 1).getTitulo()) > 0) {
					aux = filmesArmazenados.get(j);
					filmesArmazenados.set(j, filmesArmazenados.get(j + 1));
					filmesArmazenados.set(j + 1, aux);
				}
			}

		}
	}

	public String buscaPorAno(int ano) {
		String str;

		for (int i = 0; i < filmesArmazenados.size(); i++) {
			if (filmesArmazenados.get(i).getAno() == ano) {

				str = "nomeDoArquivo:"
						+ filmesArmazenados.get(i).getNomeDoArquivo()
						+ "\n Diretor:" + filmesArmazenados.get(i).getDiretor()
						+ "\n Atores Principais: "
						+ filmesArmazenados.get(i).getAtoresPrinc()
						+ "\n Durac�o: "
						+ filmesArmazenados.get(i).getDuracao() + "\n ano: "
						+ filmesArmazenados.get(i).getAno() + "\n Idioma: "
						+ filmesArmazenados.get(i).getIdioma() + "\n T�tulo: "
						+ filmesArmazenados.get(i).getTitulo()
						+ "\n Descricao: "
						+ filmesArmazenados.get(i).getDescricao()
						+ "\n G�nero: " + filmesArmazenados.get(i).getGenero();
				return str;
			}

		}
		return null;
	}
	/**
	 * Metodo que cria o arquivo binario.
	 * @param nomeArquivo
	 * @throws IOException
	 */
	public void criarArquivo(String nomeArquivo) throws IOException {

		ObjectOutputStream obj;

		obj = new ObjectOutputStream(new FileOutputStream(new File(nomeArquivo)));

		obj.writeObject(filmesArmazenados);

		obj.close();

	}

	/**
	 * M�todo que le o arquivo e trata a excessao se for preciso.
	 * @param nomeArquivo
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */

	public void lerArquivo(String nomeArquivo) throws FileNotFoundException, IOException, ClassNotFoundException {

		ObjectInputStream obj;

		obj = new ObjectInputStream(new FileInputStream(new File(nomeArquivo)));

		ArrayList<Filme> readObject = (ArrayList<Filme>) obj.readObject();
		filmesArmazenados = readObject;

		obj.close();

	}
}