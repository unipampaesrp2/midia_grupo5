package modelo.dados;

import java.io.*; 
import java.util.*;

import modelo.entidade.Musica;

/**
 * Classe de manipula��o de dados
 * 
 * @author MatheusLe�o
 *
 */
public class ControlaMusica<E> {

	Set<Musica> musicas;

	Scanner entrada = new Scanner(System.in);

	public ControlaMusica() {

		musicas = new HashSet<Musica>();

	}

	/**
	 * m�todo para adicionar uma musica no ArrayList recebendo como par�metro um
	 * objeto criado com informa��es passadas pelo usu�rio
	 * � usado um if para testar, porque a Cole��o SET n�o aceita elementos duplicados
	 * caso o elemento passado j� exista, retorna false,s en�o inclui a nova musica 
	 * @param m
	 */

	public boolean AdicionaMusica(Musica m) {

		if(musicas.add(m)){
			return true;
		}
		
		return false;
		

	}

	/**
	 * M�todo para alterar uma determinada musica recebe como par�metro o titulo
	 * e um objeto, entra no la�o e vai iterando at� encontrar a musica com o
	 * t�tulo passado pelo usu�rio ap�s isso � feito a troca do objeto antigo
	 * pelo novo objeto passado pelo usu�rio e retorna true para onde o m�todo
	 * foi chamado caso a musica seja alterada com sucesso, ou false caso
	 * contr�rio
	 * 
	 * @param titulo
	 * @param m
	 * @return
	 */

	public boolean alteraMusica(String titulo, Musica m) {
		for (Musica musica : musicas) {
			if (musica.getTitulo().equalsIgnoreCase(titulo)) {

				musicas.remove(musica);
				musicas.add(m);
				return true;
			}

		}
		return false;

	}

	/**
	 * M�todo para excluir uma determinada musica recebe como par�metro o t�tulo
	 * da musica entra no la�o procurando a musica, quando encontrar a musica �
	 * removida e retorna true, caso contr�rio retorna false
	 * 
	 * @param titulo
	 * @return
	 */

	public boolean remove(String titulo) {
		for (Musica musica : musicas) {
			if (musica.getTitulo().equalsIgnoreCase(titulo)) {

				musicas.remove(musica);
				return true;
			}

		}
		return false;
	}

	/**
	 * M�todo para ler todas musicas do ArrayList retorna uma String com todas
	 * as informa��es do ArrayList se o Array estiver vazio, retorna null
	 * 
	 * @return
	 */

	public String getMusicas() {

		if (musicas.size() > 0) {

			String str = "";
			for (int i = 0; i < musicas.size(); i++) {

				str += (i + 1) + "� Musica: \n  ";
				str += musicas.toString();
				str += "\n\n";

			}
			return str;
		}
		return null;
	}

	/**
	 * M�todo criado para auxiliar na hora da altera��o, exclus�o e etc, para
	 * validar a exist�ncia da musica recebe como par�metro o t�tulo e percorre
	 * o ArrayList procurando a informa��o que foi passada por par�metro retorna
	 * tru caso a musica exista, ou false caso contr�rio
	 * 
	 * @param titulo
	 * @return
	 */

	public boolean existe(String titulo) {
		for (Musica musica : musicas) {

			if (musica.getTitulo().equalsIgnoreCase(titulo)) {

				return true;
			}

		}
		return false;

	}

	/**
	 * 
	 * @param nomeArquivo
	 * @throws IOException
	 */
	public void criarArquivo(String nomeArquivo) throws IOException {

		ObjectOutputStream obj;

		obj = new ObjectOutputStream(new FileOutputStream(new File(nomeArquivo)));

		obj.writeObject(musicas);

		obj.close();

	}

	/**
	 * 
	 * @param nomeArquivo
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */

	public void lerArquivo(String nomeArquivo) throws FileNotFoundException, IOException, ClassNotFoundException {

		ObjectInputStream obj;

		obj = new ObjectInputStream(new FileInputStream(new File(nomeArquivo)));

		HashSet<Musica> readObject = (HashSet<Musica>) obj.readObject();
		musicas = readObject;

		obj.close();

	}
	
	
}
