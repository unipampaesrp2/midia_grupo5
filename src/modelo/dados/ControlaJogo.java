package modelo.dados;

/**
 * 
 */
import java.io.*;
import java.util.*;

import modelo.entidade.Jogo;
import modelo.entidade.Musica;

/**
 * 
 * @author eduardo
 * 
 * @param <E>
 */
public class ControlaJogo<E> {
	/**
     * 
     */
	private Vector<Jogo> jogosArmazenados;
	
	Scanner entrada = new Scanner(System.in);

	/**
     * 
     */
	public ControlaJogo() {

		jogosArmazenados = new Vector<Jogo>();

	}

	/**
	 * 
	 * @param jogos
	 * @return
	 */
	private Jogo[] getVetorDeJogo(Vector<Jogo> jogos) {
		Jogo[] vectorJogos = new Jogo[jogos.size()];
		for (int count = 0; count < jogos.size(); count++) {
			vectorJogos[count] = jogos.get(count);
		}
		return vectorJogos;
	}

	/**
     * 
     */
	private void ordenarElementos() {
		OrdenacaoJogo quickSort = new OrdenacaoJogo();
		this.jogosArmazenados = quickSort.sort(jogosArmazenados);
	}

	/**
	 * 
	 * @param j
	 * Metodo que recebe um objeto Jogo J e adiciona a Array JogosArmazenados
	 */
	public void AdicionaJogo(Jogo j) {

		jogosArmazenados.add(j);

	}
	
	

	/**
	 * 
	 * @param titulo
	 * @param j
	 * 
	 * @return
	 */
	
	public boolean alteraJogo(String titulo, Jogo j) {
		for (Jogo jogo : jogosArmazenados) {
			if (jogo.getTitulo().equalsIgnoreCase(titulo)) {
				jogosArmazenados.remove(jogo);
				jogosArmazenados.add(j);
				return true;
			}

		}
		return false;

	}

	/**
	 * 
	 * @param titulo
	 * @return
	 */
	public boolean exclui(String titulo) {

		for (Jogo jogo : jogosArmazenados) {
			if (jogo.getTitulo().equalsIgnoreCase(titulo)) {

				jogosArmazenados.remove(jogo);
				return true;
			}

		}
		return false;
	}

	/**
	 * 
	 * @return
	 */
	public String ListaJogos() {

		//ordenarElementos();

		String str = "";

		if (jogosArmazenados.size() > 0) {

			str += jogosArmazenados.toString();
			
			return str;
		}else{
			
			return null;
		}
		
	}
	
	/**
	 * 
	 * @param titulo
	 * @return
	 */
	public boolean existe(String titulo) {
		for (Jogo jogo : jogosArmazenados) {

			if (jogo.getTitulo().equalsIgnoreCase(titulo)) {

				return true;
			}

		}
		return false;

	}
	public boolean existeA(String nomeDoArquivo) {
		for (Jogo jogo : jogosArmazenados) {

			if (jogo.getTitulo().equalsIgnoreCase(nomeDoArquivo)) {

				return true;
			}

		}
		return false;

	}

	/**
	 * 
	 * @param nomeArquivo
	 * @throws IOException
	 */
	public void criarArquivo(String nomeArquivo) throws IOException {

		ObjectOutputStream obj;

		obj = new ObjectOutputStream(
				new FileOutputStream(new File(nomeArquivo)));

		obj.writeObject(jogosArmazenados);

		obj.close();

	}

	/**
	 * 
	 * @param nomeArquivo
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void lerArquivo(String nomeArquivo) throws FileNotFoundException,
			IOException, ClassNotFoundException {

		ObjectInputStream obj;

		obj = new ObjectInputStream(new FileInputStream(new File(nomeArquivo)));

		Vector<Jogo> readObject = (Vector<Jogo>) obj.readObject();
		jogosArmazenados = readObject;

		obj.close();

	}

	/**
	 * 
	 * @param genero
	 * @return
	 */
	public String buscaPorGenero(String genero) {
		String str;
		for (int i = 0; i < jogosArmazenados.size(); i++) {
			if (jogosArmazenados.get(i).getGenero().equals(genero)) {
				str = "nomeDoArquivo:"
						+ jogosArmazenados.get(i).getNomeDoArquivo()
						+ "\n Titulo:" + jogosArmazenados.get(i).getTitulo()
						+ "\n Descricão: "
						+ jogosArmazenados.get(i).getDescricao()
						+ "\n Genero: " + jogosArmazenados.get(i).getGenero()
						+ "\n Autores: " + jogosArmazenados.get(i).getAutores()
						+ "\n Ano: " + jogosArmazenados.get(i).getAno()
						+ "\n Nº Jogadores: "
						+ jogosArmazenados.get(i).getnJogadores() + "\n Rede: "
						+ jogosArmazenados.get(i).issRede();

				return str;
			}

		}
		return null;
	}

	/**
	 * 
	 * @param ano
	 * @return
	 */
	public String buscaPorAno(int ano) {
        
		String str;

		for (int i = 0; i < jogosArmazenados.size(); i++) {
			
			if (jogosArmazenados.get(i).getAno() == ano) {

				str = jogosArmazenados.toString();

				return str;
			}
		}
		return null;
	}

	/**
	 * 
	 * @param titulo
	 * @return
	 */
	public String buscaPorTitulo(String titulo) {
		String str;
		for (int i = 0; i < jogosArmazenados.size(); i++) {

			if (jogosArmazenados.get(i).getTitulo().equals(titulo)) {
				str = "nomeDoArquivo:"
						+ jogosArmazenados.get(i).getNomeDoArquivo()
						+ "\n Titulo:" + jogosArmazenados.get(i).getTitulo()
						+ "\n Descricão: "
						+ jogosArmazenados.get(i).getDescricao()
						+ "\n Genero: " + jogosArmazenados.get(i).getGenero()
						+ "\n Autores: " + jogosArmazenados.get(i).getAutores()
						+ "\n Ano: " + jogosArmazenados.get(i).getAno()
						+ "\n Nº Jogadores: "
						+ jogosArmazenados.get(i).getnJogadores() + "\n Rede: "
						+ jogosArmazenados.get(i).issRede();

				return str;
			}

		}
		return null;
	}
}
